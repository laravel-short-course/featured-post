<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route to return a view where user can create a post
Route::get('/posts/create', [PostController::class, 'create']);

// Route for a route wherein form data can be sent via POST method
Route::post('/posts', [PostController::class, 'store']);

// Route that will return a view containing all posts
Route::get('/posts', [PostController::class, 'index']);

Auth::routes();

Route::get('/home', [HomeController::class, 'index']);

Route::get('/', [PostController::class, 'welcome']);

// Route that will return a view containing only the authenticated user's posts.
Route::get('/posts/myPosts', [PostController::class, 'myPosts']);

// Specific URL parameter's id
Route::get('/posts/{id}', [PostController::class, 'show']);

Route::get('/posts/{id}/edit', [PostController::class, 'edit']);

// Route that will overwrite an existing post with the form data submitted by the user.
Route::put('posts/{id}', [PostController::class, 'update']);

Route::delete('posts/{id}', [PostController::class, 'archive']);

Route::put('/posts/{id}/like', [PostController::class, 'like']);

Route::post('/post/{id}/comment', [PostController::class, 'comment']);
