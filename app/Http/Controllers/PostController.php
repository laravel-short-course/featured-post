<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{
    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {
        // If there is an authenticated user
        if (Auth::user()) {
            // Create a new Post object from the post model
            $post = new Post;
            // Define the properties of the $post object using received form data
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            // Get the id of the authenticated user and set it as the value of the user_id column
            $post->user_id = (Auth::user()->id);
            // Save the post object to the database
            $post->save();

            return redirect('/posts');
        } else {
            return redirect('/login');
        }
    }

    public function index()
    {
        // Get all posts from the database
        $posts = Post::where('isActive', 1)->get();
        return view('posts.index')->with('posts', $posts);
    }

    public function welcome()
    {
        $posts = Post::inRandomOrder()->limit(3)->get();
        return view('welcome')->with('posts', $posts);
    }

    public function myPosts()
    {
        // If the user is logged in
        if (Auth::user()) {
            $posts = Auth::user()->posts; // Retrieve the user's own posts

            return view('posts.index')->with('posts', $posts);
        } else {
            return redirect('/login');
        }
    }

    public function show($id)
    {
        $post = Post::find($id);

        return view('posts.show')->with('post', $post);
    }

    public function edit($id)
    {
        $post = Post::find($id);

        return view('posts.edit')->with('post', $post);
    }

    // Pass both the form data in the request, as well as the id of the post to be updated
    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        // If authenticared user's id is the same as the post's user_id
        if (Auth::user()->id == $post->user_id) {
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->save();
        }
        return redirect('/posts');
    }

    public function destroy($id)
    {
        $post = Post::find($id);

        if (Auth::user()->id == $post->user_id) {
            $post->delete();
        }
        return redirect('/posts');
    }

    public function archive($id)
    {
        $post = Post::find($id);
        if (Auth::user()->id == $post->user_id) {
            $post->isActive = 0;
            $post->save();
        }
        return redirect('/posts');
    }

    public function like($id)
    {
        $post = Post::find($id);
        $user_id = Auth::user()->id;
        // Check if the authenticated user is NOT the post author.
        if ($post->user_id != $user_id) {
            // Check if a post has already been liked by this user
            if ($post->likes->contains("user_id", $user_id)) {
                // Delete the like made by the user to the unlike the post
                PostLike::where("post_id", $post->id)->where("user_id", $user_id)->delete();
            } else {
                // If the user hasn't liked the post yet
                $postLike = new PostLike;
                // Define the properties of the postLike object
                $postLike->post_id = $post->id;
                $postLike->user_id = $user_id;

                $postLike->save();
            }
            return redirect("/posts/$id");
        }
    }

    public function comment(Request $request, $id)
    {
        $post = Post::find($id);
        $user_id = Auth::user()->id;

        $postComment = new PostComment;
        $postComment->post_id = $post->id;
        $postComment->user_id = $user_id;
        $postComment->content = $request->input('content');
        $postComment->save();

        return redirect("/posts/$id");
    }
}
