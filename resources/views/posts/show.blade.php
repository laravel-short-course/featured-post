@extends('layouts.app')

@section('content')
    <div class = "card">
        <div class = "card-body">
            <h2 class="card-title">{{$post->title}}</h2>
            <p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
            <p class="card-subtitle text-muted">Likes: {{count($post->likes)}}</p>
            <p class="card-subtitle text-muted mb-3">Created at: {{$post->created_at}}</p>
            <p class="card-text">{{$post->content}}</p>
            @if (Auth::id() != $post->user_id)
                <form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
                    @method('PUT')
                    @csrf
                    @if ($post->likes->contains("user_id", Auth::id()))
                        <button type="submit" class="btn btn-danger">Unlike</button>
                    @else
                        <button type="submit" class="btn btn-success">Like</button>
                    @endif
                </form>
            @endif
                <!-- Modal button -->
			<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#commentModal">
				Post Comment
			</button>
            <!-- Modal -->
	        <div class="modal fade" id="commentModal" tabindex="-1" aria-labelledby="commentModalLabel" aria-hidden="true">
		    <div class="modal-dialog">
			    <div class="modal-content">
				    <div class="modal-header">
					    <h5 class="modal-title" id="commentModalLabel">Post a Comment</h5>
					    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				    </div>
				    <div class="modal-body">
                    <div class="mt-3">
                        <a href="/posts" class="card-link">View all posts</a>
                    </div>
                </div>
            </div>
            <form method="POST" action="/posts/{{$post->id}}/comment">
                @csrf
                <div class="form-group mb-3">
                    <label for="content">Content:</label>
                    <textarea class="form-control" id="content" name="content" rows="3"></textarea>
                 </div>
                <button type="submit" class="btn btn-primary">Post Comment</button>
            </form>
@endsection